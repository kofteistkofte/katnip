from django.db.models import Q
from django.views.generic import ListView, DetailView, CreateView, DeleteView
from django.http.response import HttpResponseRedirect
from meow.models import Meow
from meow.forms import MeowForm


class MeowIndexView(ListView):
    model = Meow
    template_name = "meow_index.html"
    context_object_name = "meow_list"

    def get_queryset(self):
        return Meow.objects.all().order_by('-pub_time')


class MeowDetailView(DetailView):
    model = Meow
    slug_field = "slug"
    template_name = "meow_detail.html"
    context_object_name = "meow"


class MeowCreateView(CreateView):
    form_class = MeowForm
    template_name = "uder_form.html"
    success_url = "/"

    def post(self, request, *args, **kwargs):
        form = MeowForm(request.POST, request.FILES)
        if form.is_valid():
            instance = form.save(commit=False)
            instance.user = request.user
            instance.save()
        return HttpResponseRedirect("/")


class MeowDeleteView(DeleteView):
    model = Meow
    success_url = "/"


class MeowSearchView(MeowIndexView):
    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['q'] = self.request.GET.get("q")
        return context

    def get_queryset(self):
        q = self.request.GET.get("q")
        return Meow.objects.filter(
                Q(body__icontains=q) |
                Q(user__username=q)
            )


class MeowHashView(MeowIndexView):
    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['q'] = self.request.GET.get("q")
        return context

    def get_queryset(self):
        q = "#" + self.request.GET.get("q")
        return Meow.objects.filter(body__icontains=q)

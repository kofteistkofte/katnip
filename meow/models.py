import random
import string
import re

from django.db import models
from django.urls import reverse
from django.contrib.auth.models import User
from django.utils.translation import gettext as _


class Meow(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE,
                            verbose_name=_("User"))
    parent = models.ForeignKey('self', on_delete=models.CASCADE,
                           null=True, blank=True,
                           verbose_name=_("Parent"))
    slug = models.SlugField(_("URL"), max_length=20,
                            null=True, blank=True)
    body = models.TextField(_("Body"), max_length=512)
    image = models.ImageField(_("Image"), upload_to="meow/%Y/%m/%d/",
                              null=True, blank=True)
    pub_time = models.DateTimeField(_("Publish Time"), auto_now_add=True)

    class Meta:
        verbose_name = "Meow"
        verbose_name_plural = _("Meows")
        ordering = ['-pub_time']


    def __str__(self):
        return "%s - %s" % (self.user, self.body[:20])

    def save(self, *args, **kwargs):
        if not self.slug:
            s = string.ascii_letters + string.digits
            while True:
                ran_slug =''.join(random.sample(s, 14))
                try:
                    is_slug = self.__class__.objects.get(slug=ran_slug)
                except Exception:
                    is_slug = None
                if not is_slug:
                    self.slug = ran_slug
                    break
                continue
            super().save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('meow:detail', args=[str(self.slug)])

    def get_body(self):
        raw = self.body
        safe = re.sub('<[^<]+?>', '', raw)
        with_mention = re.sub('@([a-z,A-Z,0-9]+)',
                              r'<a href="/u/\1">@\1</a>',
                              safe)
        with_hash = re.sub('\#([a-z,A-Z,0-9]+)',
                              r'<a href="/h/?q=\1">#\1</a>',
                              with_mention)
        with_url = re.sub(r'((http[s]?:\/\/)?[^\s(["<,>]*\.[^\s[",><]*)',
                              r'<a href="//\1" target="_blank">\1</a>',
                              with_hash)
        return with_url

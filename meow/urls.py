from django.urls import path

from meow import views

app_name = "meow"

urlpatterns = [
    path('', views.MeowIndexView.as_view(), name="index"),
    path('search/', views.MeowSearchView.as_view(), name="search"),
    path('h/', views.MeowHashView.as_view(), name="hash"),
    path('meow_post/', views.MeowCreateView.as_view(), name="create"),
    path('meow/<slug:slug>/', views.MeowDetailView.as_view(), name="detail"),
    path('meow/<slug:slug>/delete/', views.MeowDeleteView.as_view(), name="delete"),
]

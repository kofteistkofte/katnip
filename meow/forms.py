from django import forms
from meow.models import Meow


class MeowForm(forms.ModelForm):
    class Meta:
        model = Meow
        fields = ['body', 'image']

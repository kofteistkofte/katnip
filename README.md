## UYARI
Bu proje ciddi bir proje dğeildir, sadece bir canlı yayında hazırlanmıştır.

## WARNING
This project is just a speedrun project that I made during a single livestream. Do not question any of the stupid desicions I made...

![Alt text](./screenshots/meow.png?raw=true "Screenshot")

## Proje Açıklaması
Proje bir canlı yayında bitmesi planlanan basit bir Twitter klonudur. Kedi teması üzerinden ilerleyip ismi Katnip olup mesajlarının adı "Meow" olacaktır

## Yapılacaklar Listesi
- [x] 512 karakter limitli mesajlar
- [x] Kullanıcı profili
- [x] Kullanıcı avatarı
- [x] Opsiyonel görsel yükleme
- [x] @ ile kullanıcı etiketleme
- [x] Basit bir bootstrap arayüz
- [x] Kullanıcı kayıt formu
- [x] Kullanıcı login formu

## Opsiyonel yapılacaklar listesi
- [x] # ile konu etiketi belirleme
- [x] Otomatik URL tanıma
- [ ] Ajax sayfa güncelleme
- [ ] Profil detayları güncelleme sayfası
- [ ] Ajax gönderi formu

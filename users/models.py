from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import gettext as _


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE,
                                verbose_name=_("User"))
    avatar = models.ImageField(_("Avatar"), upload_to="avatars/",
                               null=True, blank=True)
    bio = models.CharField(_("User Bio"), max_length=512,
                           null=True, blank=True)

    class Meta:
        verbose_name = _("Profile")
        verbose_name_plural = _("Profiles")

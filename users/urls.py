from django.urls import path
from users import views

app_name = "users"

urlpatterns = [
    path('login/', views.SiteLoginView.as_view(), name="login"),
    path('logout/', views.SiteLogoutView.as_view(), name="logout"),
    path('register/', views.SiteRegisterView.as_view(), name="register"),
    path('update/', views.ProfileUpdateView.as_view(), name="create"),
    path('u/<slug:slug>/', views.UserDetailView.as_view(), name="detail"),
]

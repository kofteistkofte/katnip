from django.views.generic import DetailView, CreateView, UpdateView, FormView
from django.contrib.auth import views as auth_views
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django.shortcuts import render
from django.http.response import HttpResponseRedirect
from django.utils.translation import gettext as _
from users.forms import ProfileForm


class UserDetailView(DetailView):
    model = User
    slug_field = "username"
    template_name = "user.html"
    context_object_name = "user"


class SiteLoginView(auth_views.LoginView):
    template_name = "user_form.html"

    def get_success_url(self):
        return "/"

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['button'] = _("Login")
        return context
    

class SiteLogoutView(auth_views.LogoutView):
    next_page = "/"


class SiteRegisterView(CreateView):
    template_name = "user_form.html"
    success_url = "/login/"
    form_class = UserCreationForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['profile_form'] = ProfileForm()
        context['button'] = _("Register")
        return context

    def post(self, request, *args, **kwargs):
        form = UserCreationForm(request.POST)
        profile_form = ProfileForm(request.POST, request.FILES)

        if form.is_valid() and profile_form.is_valid():
            new_user = form.save()
            new_profile = profile_form.save(commit=False)
            new_profile.user = new_user
            new_profile.save()
            return HttpResponseRedirect("/login/")
        else:
            self.object = None
            cnt = self.get_context_data()
            return render(request, self.template_name, cnt)



class ProfileUpdateView(UpdateView):
    template_name = "user_form.html"
    success_url = "/"
    form_class = ProfileForm

    def get_object(self):
        return self.request.user.profile

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['button'] = _("Update")
        return context

from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.models import User
from users.models import Profile

admin.site.unregister(User)


class ProfileInline(admin.TabularInline):
    model = Profile
    extra = 1
    max_num = 1


@admin.register(User)
class UserAdmin(BaseUserAdmin):
    inlines = [ProfileInline]
